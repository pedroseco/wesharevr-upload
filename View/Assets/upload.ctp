

<div class="row">
	<div class="col-lg-12">
		</div>
		<div class="uploader-wrapper">
			<div id="pluploader" style="margin-top:120px">
				<a id="pickfiles" href="javascript:;" class="btn btn-success btn-lg">SELECT FILE TO UPLOAD</a>
			</div>
			<div id="uploadProgress"> </div>
			<div id="responseUploader"> </div>
		</div>
	</div>
</div>


<script type="text/javascript">
var ts, destination, prefix='<?php echo Configure::read('Aws.prefix')?>',folder='<?php echo $this->Session->read('Auth.User.id')?>';
$(function() {
	var uploader = new plupload.Uploader({
        	multi_selection:false,
		preinit : {
			UploadFile: function(up, file) {
				ts = Math.round(new Date().getTime() / 1000);
				destination = prefix+'/'+ts+'/'+file.name;
				up.settings.multipart_params.key = destination;
			}
		},
		init: {
			FilesAdded: function(up, files) {
				//Start the file upload automatically
          			up.start();
          			$('#pickfiles').hide();
        		},
			FileUploaded: function(up, file, info) {
			},
			UploadProgress: function(up, file) {
				document.getElementById('uploadProgress').innerHTML = '<span class="big-numbers">' + file.percent + "%</span>";
			},
			UploadComplete: function(up, file, info) {
	                // this is the file path @ aws (without the 'Uploads/' prefix)
	                filename = 'http://wesharevr.com/storage/assets/download/'+ts + '/' + file[0]['name']
	                $('#wpInputTarget', window.parent.document).val(filename);

	                var successText = '<h2>Your file has been successfully uploaded. <br> You can now close this window.</h2>';
	                $('#responseUploader').html(successText);
			},
		},
		runtimes : 'html5,flash,silverlight',
                        drop_element : 'pluploader',
		browse_button : 'pickfiles',
		container: document.getElementById('pluploader'),
		url : 'http://<?php echo Configure::read('Aws.bucket'); ?>.s3.amazonaws.com/',
		multipart: true,
		multipart_params: {
			'key': '${filename}',
			'Filename': '${filename}',
			'acl': 'private',
			'Content-Type': 'video/*',
			'AWSAccessKeyId': '<?php echo Configure::read('Aws.key'); ?>',
			'policy': '<?php echo $policy; ?>',
			'signature': '<?php echo $signature; ?>'
		},
		filters : {
			// Maximum file size
			max_file_size : '10000mb',
			// Specify what files to browse for
			mime_types: [
				{title : "Movie files", extensions : "<?php echo Configure::read('App.allowed_file_types');?>"}
			]
		},
		flash_swf_url : '/bower_components/plupload/js/Moxie.swf',
		silverlight_xap_url : '/bower_components/plupload/js/Moxie.xap'
	});

	uploader.init();
});
</script>

<?php echo $this->Html->script(array(
    '/bower_components/plupload/js/plupload.full.min.js'
   ));?>
