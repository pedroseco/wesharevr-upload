<?php

/**
 * assets controller
 *
 * package Uploader
 */

require_once("../Vendor/autoload.php");

use Aws\S3\S3Client;

class AssetsController extends AppController {


/**
 * upload a new asset
 */
    public function upload()
    {

        $this->checkKey();
        $policy = $this->Asset->generatePolicy();
        $signature = $this->Asset->generateSignature($policy);
        $this->set(compact('policy','signature'));

    }

/**
 * download a file
 *
 * @var $ts timestamp folder
 * @var $fn filename
 * @var $key to check against
 */
    public function download($ts=null, $fn=null, $key=null)
    {
        $match = sha1(Configure::read('App.key').$ts.$fn);

        if($match != $key) {
            throw new ForbiddenException();
        } else {
            $s3 = S3Client::factory(array(
            'key' => Configure::read('Aws.key'),
            'secret' => Configure::read('Aws.secret')
          ));

         $signedUrl = $s3->getObjectUrl(Configure::read('Aws.bucket'),
                                            Configure::read('Aws.prefix'). '/' . $ts . '/' . $fn,
                                            Configure::read('Aws.lifetime'));

         $this->redirect($signedUrl);
         exit;

        }
    }


}
