<?php
class Asset extends AppModel {
    public $useTable  = false;

/**
 * generate an AWS policy for uploads
 *
 * @return String generated policy
 */
    public function generatePolicy()
    {

        return base64_encode(json_encode(array(
            'expiration' => date('Y-m-d\TH:i:s.000\Z', strtotime('+1 day')),
            'conditions' => array(
                    array('bucket' => Configure::read('Aws.bucket')),
                    array('acl' => 'private'),
                    array('starts-with', '$key', ''),
                    array('starts-with', '$Content-Type', ''),
                    array('starts-with', '$name', ''),
                    array('starts-with', '$Filename', ''),
        ))));
    }

/**
 * generate an AWS signature for uploads
 *
 * @return String generated signature
 */

    public function generateSignature($policy=null){
        return base64_encode(hash_hmac('sha1', $policy, Configure::read('Aws.secret'), true));
    }



}
